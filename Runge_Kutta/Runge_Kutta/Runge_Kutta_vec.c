#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#define retard 1
#define mass 1
#define gravity 10
int n;
double* f1(double, double*, int, double*);
double** Runge_Kutta(double*(*f)(double, double*), double t0, double tn, double* y0, int l, double h);
void put(double* a, double* b, int l)
{
	int i = 0;
	for (; i < l; i++)
	{
		a[i] = b[i];
	}
}void clear(double* temp, int len)
{
	int i = 0;
	for (; i < len; i++)
		temp[i] = 0;
}
double* sum(double* y, double* operand, int l, double* temp)
{
	int len = l;
	for (int i = 0; i < len; i++)
	{
		temp[i] = y[i] + operand[i];
	}
	return temp;
}
double* multi(double* y, double operand, int l, double* temp)
{
	int len = l;
	for (int i = 0; i < len; i++)
	{
		temp[i] = y[i] * operand;
	}
	return temp;
}
int main()
{
	int i, j;
	double h = 1;
	double y[2] = { 0, 2};
	double** data = Runge_Kutta(f1, 0, 8, y, 2, 0.00001);
	FILE* file;
	file = fopen("data2.txt", "w");
	for (i = 1; i < n; i++)
	{
		fprintf(file, "%lf %lf %lf\n", 0.00001*i, data[i][0], data[i][1]);
	}
	for (i = 0; i < n + 1; i++)
		free(data[i]);
	free(data);
	printf("end");
	getchar();
	return 0;
}

double** Runge_Kutta(double*(*f)(double, double*), double t0, double tn, double* y0, int l, double h)
{
	n = (tn - t0) / h;
	int len = l;
	int i = 0, j;
	double* k1, *k2, *k3, *k4;
	k1 = (double*)malloc(sizeof(double)*len);
	k2 = (double*)malloc(sizeof(double)*len);
	k3 = (double*)malloc(sizeof(double)*len);
	k4 = (double*)malloc(sizeof(double)*len);
	double t = t0, *y;
	y = (double*)malloc(sizeof(double)*len);
	put(y, y0, len);
	double** data = (double**)malloc(sizeof(double*)*(n + 1));
	double* temp = (double*)malloc(sizeof(double)*len);
	double* temp1 = (double*)malloc(sizeof(double)*len);
	for (j = 0; j < n + 1; j++)
	{
		data[j] = (double*)malloc(sizeof(double)*len);
	}
	data[0][0] = n;
	put(data[1], y0, len);
	for (i = 2; i < n; i++)
	{
		clear(temp, len);
		put(k1, f(t, y, len, temp), len);
		clear(temp, len);
		put(k2, f(t + h / 2, sum(y, multi(k1, (h / 2), len, temp), len, temp), len, temp), len);
		clear(temp, len);
		put(k3, f(t + h / 2, sum(y, multi(k2, (h / 2), len, temp), len, temp), len, temp), len);
		clear(temp, len);
		put(k4, f(t + h, sum(y, multi(k3, h, len, temp), len, temp), len, temp), len);
		clear(temp, len);
		clear(temp1, len);
		put(data[i], sum(data[i - 1], multi((sum(sum(sum(k1, multi(k2, 2, len, temp1), len, temp1), multi(k3, 2, len, temp), len, temp), k4, len, temp)), (h / 6), len, temp), len, temp), len);
		t = t + h;
		put(y, data[i], len);
	}
	free(k1);
	free(k2);
	free(k3);
	free(k4);
	free(temp);
	free(temp1);
	return data;
}
double* f1(double t, double* y, int l, double* temp)
{
	temp[0] = -sin(y[1]);
	temp[1] = y[0];
	return temp;
}
