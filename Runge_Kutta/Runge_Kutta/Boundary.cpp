/*#include<iostream>
#include<string>
#include<fstream>
#include<cmath>
using namespace std;

class floatArray {
public:
	floatArray() : ar(NULL), length(0) {	}
	void print(); // ar 의 모든 내용을 출력한다.
	double* ar; // input file을 읽어 배열을 동적할당한다.
protected:
	int length; // 데이터 개수
};

void floatArray::print()
{
	for (int i = 0; i < length; i++)
	{
		cout << ar[i];
	}
	cout << endl;
}

class Matrix : floatArray
{
public:
	Matrix(double x1, double y1, double x2, double y2, double (*p1)(double), double(*r1)(double), double(*f1)(double), double h1)
	{
		x0 = x1; xe = x2; y0 = y1; ye = y2; p = p1; r = r1; h = h1; f = f1;
		n = (x2 - x1) / h1;
		n = n - 1;
		length = (n)*(n);
		ar = new double[length];
		for (int i = 0; i < length; i++)
		{
			ar[i] = 0;
		}
	}
	~Matrix()
	{
		delete[] ar;
		delete[] u;
	}
	void cal();
	void add(Matrix&);
	void sub(Matrix&);
	void mul(Matrix&);
	void inverse();
	float getdata(int i) const
	{
		return ar[i];
	}
	void print()
	{
		for (int i = 0; i < n; i++)
		{
			for (int j = 0; j < n; j++)
				outfile << ar[i*n + j] << ' ';
			outfile << endl;
		}
		outfile << endl;
	}
	void printu()
	{
		for (int i = 0; i < n; i++)
		{
			outfile << x0 + (i+1)*h << '\t' << u[i] << endl;
		}
		outfile << endl;
	}
	void printu(ofstream& out)
	{
		for (int i = 0; i < n; i++)
		{
			out << x0 + (i + 1)*h << '\t' << u[i] << endl;
		}
		out << endl;
	}
private:
	double x0, xe, y0, ye;
	double h;
	double(*p)(double);
	double(*r)(double);
	double(*f)(double);
	double *u;
	static ofstream outfile;
	int n;
	int num = 0;
	int j = 0;
};
ofstream Matrix::outfile("Output_1.txt");

void Matrix::add(Matrix& b)
{
	for (int i = 0; i < length; i++)
		ar[i] = ar[i] + b.getdata(i);
}
void Matrix::sub(Matrix& b)
{
	for (int i = 0; i < length; i++)
		ar[i] = ar[i] - b.getdata(i);
}
void Matrix::mul(Matrix& b)
{
	int sum = 0;
	float *c = new float[length];
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			for (int k = 0; k < n; k++)
				sum = sum + ar[i*n + k] * (b.getdata(k*n + j));
			c[i*n + j] = sum;
			sum = 0;
		}
	}
	for (int i = 0; i < length; i++)
		ar[i] = c[i];
	delete[] c;
}
void Matrix::inverse()
{
	double *b = new double[length];
	double *c = new double[length];
	double temp;
	for (int i = 0; i < length; i++)
	{
		b[i] = ar[i];
	}
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
		{
			if (i == j)
				c[i*n + j] = 1;
			else
				c[i*n + j] = 0;
		}

	try {
		for (int i = 0; i < n; i++)
		{
			if (b[i*(n + 1)] == 0)
			{
				for (int j = i + 1; j < n; j++)
					if (b[i + j*n] != 0) // 역행렬 존재?
					{
						for (int k = i; k < n; k++)
						{
							temp = b[i*n + k];
							b[i*n + k] = b[k + j*n];
							b[k + j*n] = temp;
							temp = c[i*n + k];
							c[i*n + k] = c[k + j*n];
							c[k + j*n] = temp;
						}
						break;
					}
				if (b[i*(n + 1)] == 0)
					throw - 100;
			}

			for (int j = i + 1; j < n; j++)
			{
				temp = -b[i + j*n] / b[i*(n + 1)]; //앞에 계수
				b[i + j*n] = 0;
				for (int k = i + 1; k < n; k++)
				{
					b[j*n + k] = b[j*n + k] + temp * b[k + i*n];//U
				}
				for (int k = 0; k < n; k++)
				{
					c[j*n + k] = c[j*n + k] + temp * c[k + i*n];//L
				}
			}
		}

		for (int i = n - 1; i >= 0; i--)
		{
			if (b[i*(n + 1)] == 0)
				throw - 100;
			for (int j = i - 1; j >= 0; j--)
			{
				temp = -b[i + j*n] / b[i*(n + 1)]; //계수
				b[i + j*n] = 0;
				for (int k = 0; k < n; k++)
				{
					c[j*n + k] = c[j*n + k] + temp * c[k + i*n]; //L
				}
			}
		}
		for (int i = 0; i < n; i++)
		{
			for (int j = 0; j < n; j++)
			{
				c[i*n + j] = c[i*n + j] / b[i*(n + 1)]; //마지막 나눠주기
			}
		}
		for (int i = 0; i < length; i++)
			ar[i] = c[i];
	}

	catch (int e)
	{
		if (e == -100)
			outfile << "unable to reverse" << endl;
	}

	delete[] b;
	delete[] c;
}
void Matrix::cal()
{
	double temp=0;
	for (int i = 0; i < n; i++)
	{
		ar[i*(n+1)] = p((i+1)*h + x0 + h / 2) + p((i + 1)*h + x0 - h / 2) + h*h*r((i + 1)*h + x0);
		if (i*(n + 1) + 1 < n*n)
			ar[i*(n + 1) + 1] = -p((i + 1)*h + x0 + h / 2);
		if ((i + 1)*(n + 1) - 1 < n*n)
			ar[(i+1)*(n + 1) - 1] = -p((i + 1)*h + x0 + h / 2);
	}
	inverse();
	double *c = new double[n];
	u = new double[n];
	c[0] = f(h + x0) + y0*p(x0 + h / 2) / (h*h);
	
	for (int i = 1; i < n - 1; i++)
	{
		c[i] = f((i + 1)*h + x0);
	}
	c[n - 1] = f((n - 1)*h + x0) + ye*p((n - 1)*h + x0 + h / 2) / (h*h);
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			temp += ar[i*n + j] * c[j];
		}
		u[i] = temp*h*h;
		temp = 0;
	}
	delete[] c;
}
double p(double x)
{
	return -x*x*x;
}

double q(double x)
{
	return 1;
}

double r(double x)
{
	return x;
}
double f(double x)
{
	return 9*x*x*x;
}
int main()
{
	Matrix m1(1, 0, exp(1), 0, p, r, f, 0.1);
	m1.cal();
	m1.printu();
	ofstream out("Output2.txt");
	Matrix m2(1, 0, exp(1), 0, p, r, f, 0.01);
	m2.cal();
	m2.printu(out);
	ofstream out2("Output3.txt");
	Matrix m3(1, 0, exp(1), 0, p, r, f, 0.001);
	m3.cal();
	m3.printu(out2);
	return 0;
}
*/