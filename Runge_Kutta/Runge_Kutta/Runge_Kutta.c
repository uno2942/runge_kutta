/*#include<stdio.h>
#include<stdlib.h>
#include<math.h>

double f1(double, double);
double* Runge_Kutta(double(*f)(double), double t0, double tn, double y0, double h);

int main()
{
	double h = 1;
	double* data = Runge_Kutta(f1, 0, 10, 0.7, 0.00001);
	FILE* file;
	file = fopen("Runge.txt", "w");
	for (int i = 0; i < data[0]; i++)
	{
		fprintf(file, "%lf\t%lf\n", i*0.00001,data[i]);
	}
	return 0;
}

double* Runge_Kutta(double(*f)(double), double t0, double tn, double y0, double h)
{
	int n = (tn - t0) / h;
	int i = 0;
	double k1, k2, k3, k4;
	double t = t0, y = y0;
	double* data = (double*)malloc(sizeof(double)*(n + 1));
	data[0] = n;
	data[1] = f(t0, y0);
	for (i = 2; i < n; i++)
	{
		k1 = f(t, y);
		k2 = f(t + h / 2, y + (h / 2)*k1);
		k3 = f(t + h / 2, y + (h / 2)*k2);
		k4 = f(t + h, y + h*k3);
		data[i] = data[i - 1] + (h / 6)*(k1 + 2 * k2 + 2 * k3 + k4);
		t = t + h;
		y = data[i];
	}
	return data;
}
double f1(double t, double y)
{
	return 3*y*(1-y);
}*/